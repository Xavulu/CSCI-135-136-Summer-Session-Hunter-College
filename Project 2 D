/* 
Author: Xavier Fernandez 
Course: CSCI 135-136 
Instructor: Gwenael Gatto 
Assignment: project 2, task d 

Program compares base dna strands from task b to their frame shifted version
*/  

#include <iostream> 
#include <string> 
#include <fstream> 
#include <cstdlib> 

using namespace std; 

char DNAbase_to_mRNAbase(char c); //takes DNA base, makes it capital and then converts to mRNA compliment 

string DNA_to_mRNA(string dna); // uses above function to convert whole strand 

string dictionary_read(ifstream &dict, string base); // performs dictionary lookup for translations
// passes dictionary ifstream object by reference and takes in a 3 letter string to find its match 

string makeAmino(ifstream &dict, string mRna); //uses code from task b's main to take mrna and transform to
//amino acid chain , pases dictionary by reference

int hamDistance(string s, string t); //calculates hamming distance between 2 dna strands 

void frameShift(ifstream &dict, string mRna, int index); //helper function for task d, 
//based on task b main

int main(){

    ifstream fin("frameshift_mutations.txt");//for task d
    if (fin.fail()) {
        cerr << "File cannot be read, opened, or does not exist.\n";
        exit(1);
    } 

    ifstream dict("codons.tsv");
	if(dict.fail()) 
	{
		cerr << "File cannot be read, opened, or does not exist.\n";
		exit(1);
	}

    string strand_1; 
    string strand_2;

    while(getline(fin, strand_1)) {
        
        getline(fin, strand_2);

        /* For task d the dna strand would have to be read one nucleotide at a time since 
        the strands aren't base 3 this time around. However the string.find() function 
        povides a workaround to this as it gives you the index of the substring you're 
        searching for in the string. We would search for AUG and from there we could then 
        read base 3 again*/ 
        /*non mutated strands however are in base three so they can be read and translated as 
        usual */
        string mRna = DNA_to_mRNA(strand_1); 
        string mutated = DNA_to_mRNA(strand_2); 
        
        int foundStart = mutated.find("AUG",0); //start searching for AUG from 0th index 
        //only the mutated strand needs to be searched, non mutant strands should still be base 3

    
        //cout << "NON-MUTANT:  ";
        frameShift(dict, mRna, 0); //starting a 0th index cause non mutants are base 3 
        //cout << endl << endl << "MUTANT:  ";
        frameShift(dict, mutated, foundStart); //starting at first occurence of AUG so 
        //we can treat it as base 3 
        //cout << endl << endl;

    }
    
    fin.close(); 
    dict.close();

    return 0; 

}  

char DNAbase_to_mRNAbase(char c){
    
    char corrected = toupper(c); 

    if (corrected == 'A'){

        return 'U';

    } 
    if (corrected == 'T'){

        return 'A';

    } 
    if (corrected == 'C'){

        return 'G';

    }  
    if (corrected == 'G'){

        return 'C';

    } 
    
} 

string DNA_to_mRNA(string dna){

    string newMRNA; 

    for(int i = 0; i < dna.length(); i++){

        newMRNA += DNAbase_to_mRNAbase(dna[i]);

    } 

    return newMRNA;

} 

string dictionary_read(ifstream &dict, string base) {
    
    string key, value;
    dict.clear(); // reset error state
    dict.seekg(0); // return file pointer to the beginning
    while (dict >> key >> value) {

        if(key == base){

            return value;

        }
       
    }
} 

string makeAmino(ifstream &dict, string mRna){ 

    bool startEncode = false;
    string amino;

    for(int i = 0; i < mRna.length(); i += 3){
        /* we want the encoding process to only begin at met and not add an acid once 
        STOP is reached , we can use a boolean to flag wether or not encoding should start*/ 

        if(dictionary_read(dict, mRna.substr(i , 3)) == "Met"){ //sub string is base 3

            startEncode = true; // if first is Met start process

        } 
        if(dictionary_read(dict, mRna.substr(i , 3)) == "Stop"){ //sub string is base 3

            startEncode = false; //if codon is ever Stop end encoding 

        }  
        if(startEncode == true){ //sub string is base 3

            amino = amino + dictionary_read(dict, mRna.substr(i , 3)); 
                
        }   
                       
    } 

    return amino;
      
}

int hamDistance(string s, string t){

    int hamming = 0; 

    for(int i = 0; i < s.length(); i++){

        if(s[i] != t[i]){

            hamming += 1;

        }

    } 

    return hamming;

}

void frameShift(ifstream &dict, string mRna, int index){ 
       
    //this is literally just the main from task b but as a function, gives more control of index
    //start which is good for using string.find()

    bool startEncode = false; 
    int metCounter = 0;

    for(int i = index; i < mRna.length(); i += 3){

        if(metCounter <= 1){  

            if(dictionary_read(dict, mRna.substr(i , 3)) == "Met"){

                startEncode = true; 
                metCounter += 1;

            } 
            if((dictionary_read(dict, mRna.substr(i , 3)) == "Stop") || (metCounter > 1)){

                startEncode = false; 

            }  
            if(startEncode == true){ 

                cout << dictionary_read(dict, mRna.substr(i , 3)); 
                
            }   
            if((startEncode == true) && dictionary_read(dict, mRna.substr(( i + 3) , 3)) != "Stop"){

                cout << "-";

            }
            
            
        } 
        
                
    }  
    cout << "\n";

}
